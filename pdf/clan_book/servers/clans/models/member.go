package models

import "time"

import "fmt"

type Member struct {
	ID       int64
	Name     string
	Surname  string
	Status   int32
	Spouse   *MemberRelationship
	Spouses  []*MemberRelationship
	Children []*Member
}

// Person 人
// 一些与族谱有关的人
type Person struct {
	ID          int64      `xorm:"not null pk autoincr INTEGER id" json:"id"`
	Name        string     `xorm:"not null VARCHAR(16)" json:"name"`
	Birthday    *time.Time `xorm:"DATE" json:"birthday"`
	BirthRecord string     `xorm:"VARCHAR(64)" json:"birth_record"`
	BirthPlace  string     `xorm:"VARCHAR(256)" json:"birthPlace"`
	Brief       string     `xorm:"not null TEXT" json:"brief"`
}

// MemberRelationship 成员关系
type MemberRelationship struct {
	ID           int64   `xorm:"not null pk autoincr INTEGER id" json:"id"`
	MemberID     int64   `xorm:"not null unique(main) INTEGER member_id" json:"-"`
	PersonID     int64   `xorm:"not null unique(main) INTEGER person_id" json:"-"`
	Person       *Person `xorm:"-" json:"person"`
	Main         bool    `xorm:"not null BOOL" json:"main"`
	IsNormalForm bool    `xorm:"not null default true BOOL" json:"isNormalForm"` //婚姻形式
}

type PdfMember struct {
	Index            int //用于进度设计
	Name             []rune
	Status           int32
	WifeNames        [][]rune
	Surname          string
	MinSize          Size
	NorSize          Size
	MaxSize          Size
	WidthOfChildren  float64
	HeightOfChildren float64
	Children         []*PdfMember
	Anchor           *Anchor
}

type Size struct {
	Width, Height float64
}

func (pm *PdfMember) PrintWidth(stopStatus int32, ft StyleType) float64 {
	if pm.Status > stopStatus {
		panic(fmt.Sprintf("%s's status is more than stop", string(pm.Name)))
	}
	width := 0.0
	switch ft {
	case FMin:
		width = pm.MinSize.Width
	case FMax:
		width = pm.MaxSize.Width
	default:
		width = pm.NorSize.Width
	}
	if pm.Status == stopStatus {
		return width
	}
	// width with children
	wwc := 0.0
	for _, child := range pm.Children {
		wwc += child.PrintWidth(stopStatus, ft)
	}

	if width < wwc {
		return wwc
	}
	return width
}
