package main

import (
	"context"
	"log"
	"time"

	"gitee.com/yunind/practice/mulver/example/echo"
	"google.golang.org/grpc"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
)

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithUnaryInterceptor(func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		method = "/v1" + method
		return invoker(ctx, method, req, reply, cc, opts...)
	}))
	conn, err := grpc.Dial(":5080", opts...)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := echo.NewEchoServerClient(conn)
	ctx := context.Background()

	// ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	// defer cancel()
	resp, err := healthpb.NewHealthClient(conn).Check(ctx, &healthpb.HealthCheckRequest{Service: "echo.EchoServer"})
	if err != nil {
		log.Fatalf("HealthCheck failed %+v", err)
	}
	if resp.GetStatus() != healthpb.HealthCheckResponse_SERVING {
		log.Fatalf("service not in serving state: %s", resp.GetStatus().String())
	}
	log.Printf("RPC HealthCheckStatus:%v", resp.GetStatus())
	for i := 0; i < 10; i++ {
		r, err := c.Fly(ctx, &echo.Offset{Y: float32(i)})
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		time.Sleep(1 * time.Second)
		log.Printf("RPC Response: %v %v", i, r)
	}
}
