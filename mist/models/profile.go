package models

type Profile struct {
	Sex  uint  `xorm:"not null default 0 INT(1)"`
	User *User `xorm:"-"`
}
