package models

type User struct {
	ID      int64    `xorm:"not null pk autoincr INTEGER"`
	Phone   string   `xorm:"not null VARCHAR(16)"`
	Name    string   `xorm:"not null VARCHAR(128)"`
	Profile *Profile `xorm:"-"`
}
