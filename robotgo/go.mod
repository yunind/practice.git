module gitee.com/yunind/practice/robotgo

go 1.13

require (
	github.com/alexbrainman/odbc v0.0.0-20190102080306-cf37ce290779 // indirect
	github.com/go-vgo/robotgo v0.0.0-20200120152355-f632105618ea // indirect
	gopkg.in/yaml.v2 v2.2.2
)
